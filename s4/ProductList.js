import React from 'react';

class ProductList extends React.Component{
   constructor(props){
        super(props);
        
    }
    
    render(){
        let items = this.props.source.map((product, index) =>{
            return <div key={index}>{product.productName}</div>
        })
        return(
            <div>
                <h1></h1>
                <div>
                {items}
                </div>
            </div>
            );
    }
}

export default ProductList;
